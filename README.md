</details>

******

<details>
<summary> Create a Kubernetes cluster using eksctl </summary>
 <br />

**steps:**

```sh
# With eksctl you create an EKS cluster with 3 Nodes and 1 Fargate profile
# Download eksctl cli tool
# Check that I have the correct credentials in ~/.aws/credentials

brew tap weaveworks/tap
brew install weaveworks/tap/eksctl

# Remember what scales up or down a node is the auto scaler, if you have less node than needed, 
# you need auto scaler in place
#  eksctl delete cluster --name demo-cluster

eksctl create cluster \
--name demo-cluster \
--version 1.27 \
--region eu-west-1 \
--nodegroup-name demo-nodes \
--node-type t2.micro \
--nodes 3 \
--nodes-min 1 \
--nodes-max 3
--kubeconfig=~/developments/devops-bootcamp/kubernetes-intro/k8s-config-files/kubeconfig.my-cluster.yaml


# create fargate profile in the cluster. It will apply for all K8s components in my-app namespace
# eksctl delete fargateprofile --cluster demo-cluster --name my-fargate-profile

kubectl create ns my-app

eksctl create fargateprofile \
--cluster demo-cluster \
--name my-fargate-profile \
--namespace my-app

# Set configuration file
export KUBECONFIG=/Users/johnikeson/Developments/devops-bootcamp/kubernetes-intro/k8s-config-files/kubeconfig.my-cluster.yaml

# Within AWS, create a Role for Fargate that will allow... 
#VM provisioned by Fargate access our account. This role will be attached to EKS ?????

# Create a Fargate Profile under the Cluster Option ?????

```
</details>

******

<details>
<summary> Deploy Mysql and phpmyadmin </summary>
 <br />

**steps**

```sh
# Create a custom value file for mysql - helm chart (helm-mysql.yaml)
helm install mysql --values helm-mysql.yaml bitnami/mysql
kubectl get pods

# Create a Deployment component - my-sql-admin.yaml
kubectl apply -f my-sql-admin.yaml

# Check the external ip address of the admin service and connect to it
kubectl get service/mysqladminservice -o wide

```

</details>

******

<details>
<summary> Deploy your Java Application with 2 replicas </summary>
 <br />

**steps**

```sh
# Since the application's image is already deployed to DockerHub
# Setup the connection to DockerHub
kubectl create secret docker-registry dockercred --docker-server=https://index.docker.io/v1/ --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email> --namespace my-app

kubectl get secret

# Create Secret for mysql-secret (secretKeyRef) and mysql-configmap (ConfigMap)
# See mysql-secret.yaml file
# See mysql-configmap.yaml
# Create both (secret and configmap)components in cluster
kubectl apply -f mysql-configmap.yaml --namespace my-app
kubectl apply -f mysql-secret.yaml --namespace my-app
kubectl apply -f my-java-app.yaml

```

</details>

******

<details>
<summary> Deploy Ingress Controller Using Helm</summary>
 <br />

**steps**

```sh
# Add the helm repo that contains ingress nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx 
# Repo already added

# Set the default namespace to my-app as new context for kubectl
kubectl config set-context --current --namespace=my-app

helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true # set gives ingress an external ip
kubectl get ingress -n my-app

```
</details>

******

<details>
<summary> Automate deployment </summary>
 <br />

**steps**

```sh
# Setup automatic deployment to the cluster in the pipeline
# Create an ECR registry for your java-app image

# Locally, on your computer: Create a docker registry secret for ECR
DOCKER_REGISTRY_SERVER=your ECR registry server - "your-aws-id.dkr.ecr.your-ecr-region.amazonaws.com"
DOCKER_USER=your dockerID, same as for `docker login` - "AWS"
DOCKER_PASSWORD=your dockerhub pwd, same as for `docker login` - get using: "aws ecr get-login-password --region {ecr-region}"

kubectl create secret -n my-app docker-registry my-ecr-registry-key \
--docker-server=$DOCKER_REGISTRY_SERVER \
--docker-username=$DOCKER_USER \
--docker-password=$DOCKER_PASSWORD

# SSH into server where Jenkins container is running
ssh -i {private-key-path} {user}@{public-ip}

# Enter Jenkins container
sudo docker exec -it {jenkins-container-id} -u 0 bash

# Install aws-cli inside Jenkins container
- Link: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

# Install kubectl inside Jenkins container
- Link: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

apt-get update
apt-get install -y apt-transport-https ca-certificates curl
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubectl

# Install envsubst tool
- Link: https://command-not-found.com/envsubst

apt-get update
apt-get install -y gettext-base

# create 2 "secret-text" credentials for AWS access in Jenkins: 
- "jenkins_aws_access_key_id" for AWS_ACCESS_KEY_ID 
- "jenkins_aws_secret_access_key" for AWS_SECRET_ACCESS_KEY    

# Create 4 "secret-text" credentials for db-secret.yaml:
- id: "db_user", secret: "my-user"
- id: "db_pass", secret: "my-pass"
- id: "db_name", secret: "my-app-db"
- id: "db_root_pass", secret: "secret-root-pass"

# Set the correct values in Jenkins for following environment variables: 
- ECR_REPO_URL
- CLUSTER_REGION

# Create Jenkins pipeline using the Jenkinsfile in this branch, in the root folder
Make sure the paths to the k8s manifest files in the "deploy" stage of the Jenkinsfile are all correct!!



```
</details>

******

<details>
<summary> Configure Autoscaling </summary>
 <br />

**steps**

```sh

```

</details>
